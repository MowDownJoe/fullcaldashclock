package com.mowdownDevelopments.fullcal;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new GeneralPreferenceFragment())
				.commit();
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class GeneralPreferenceFragment extends PreferenceFragment {

		private String getHoursPrefLabel(String value) {
			int realValue;
			try {
				realValue = Integer.parseInt(value);
			} catch (NumberFormatException e) {
				realValue = -1;
				Log.e("DesCal SettingsFragment",
						"Invalid parameter passed in for getHoursPrefLabel", e);
			}
			String[] displayedValues = getActivity().getResources()
					.getStringArray(R.array.hoursBeforeLabels);
			switch (realValue) {
			case 1:
				return displayedValues[0];
			case 2:
				return displayedValues[1];
			case 3:
				return displayedValues[2];
			case 6:
				return displayedValues[3];
			case 12:
				return displayedValues[4];
			case 24:
				return displayedValues[5];
			case 48:
				return displayedValues[6];
			case 72:
				return displayedValues[7];
			case 144:
				return displayedValues[8];
			case 288:
				return displayedValues[9];
			}
			return null;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_general);
			Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
					"mailto", getString(R.string.emailAddress), null))
					.putExtra(Intent.EXTRA_SUBJECT,
							"Hey, I got something to say to you...");
			findPreference("email").setIntent(intent);
			findPreference("hoursToReveal").setOnPreferenceChangeListener(
					new OnPreferenceChangeListener() {

						@Override
						public boolean onPreferenceChange(
								Preference preference, Object newValue) {
							String newSummary = getHoursPrefLabel((String) newValue);
							if (newSummary != null) {
								preference.setSummary(newSummary);
								return true;
							}
							return false;
						}
					});
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			String actuallyAnInteger = PreferenceManager
					.getDefaultSharedPreferences(getActivity()).getString(
							"hoursToReveal", "1");
			findPreference("hoursToReveal").setSummary(
					getHoursPrefLabel(actuallyAnInteger));
		}
	}
}
