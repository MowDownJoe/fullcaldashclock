package com.mowdownDevelopments.fullcal;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.CalendarContract.Events;
import android.text.format.DateUtils;
import android.util.Log;

import com.google.android.apps.dashclock.api.DashClockExtension;
import com.google.android.apps.dashclock.api.ExtensionData;

public class FullCalService extends DashClockExtension {
	public static final String[] FIELDS = { Events._ID, Events.TITLE,
			Events.ALL_DAY, Events.EVENT_LOCATION, Events.DTSTART,
			Events.EVENT_TIMEZONE, Events.DESCRIPTION };

	public FullCalService() {
	}

	@Override
	protected void onUpdateData(int arg0) {
		TimeZone tz = TimeZone.getDefault();
		long currentTimeMillis = Calendar.getInstance().getTimeInMillis() - tz.getRawOffset();
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		Cursor c;
		if (prefs.getBoolean("allDayAllowed", false)) {
			c = getContentResolver().query(
					Events.CONTENT_URI,
					FIELDS,
					new StringBuilder().append("(").append(Events.DTSTART)
							.append(" >= ?)").toString(),
					new String[] { Long.toString(currentTimeMillis) },
					Events.DTSTART, null);
		} else {
			c = getContentResolver().query(
					Events.CONTENT_URI,
					FIELDS,
					new StringBuilder().append("((").append(Events.ALL_DAY)
							.append("= ?) AND (").append(Events.DTSTART)
							.append(" >= ?))").toString(),
					new String[] { Integer.toString(0),
							Long.toString(currentTimeMillis) }, Events.DTSTART,
					null);
		}
		if (c.moveToFirst()) {
			long eventTimeMillis = c.getLong(c.getColumnIndex(Events.DTSTART));
//			if (tz.inDaylightTime(new Date(eventTimeMillis))) {
//				eventTimeMillis += tz.getDSTSavings();
//			}
			//Log.d("DesCal service", "Value of hoursToReveal: "+prefs.getString("hoursToReveal", "1"));
			if (eventTimeMillis < currentTimeMillis + 360000
					* Integer.parseInt(prefs.getString("hoursToReveal", "1"))) {
				String title = c.getString(c.getColumnIndex(Events.TITLE));
				String loc = c.getString(c
						.getColumnIndex(Events.EVENT_LOCATION));
				String time = DateUtils.formatDateTime(this, eventTimeMillis,
						DateUtils.FORMAT_SHOW_TIME);
				String desc = c.getString(c.getColumnIndex(Events.DESCRIPTION));
				StringBuilder expandedBody = new StringBuilder(time);
				if (!loc.isEmpty()){
					expandedBody.append(" - ").append(loc);
				}
				expandedBody.append("\n").append(desc);
				String uri = new StringBuilder(
						"content://com.android.calendar/events/").append(
						c.getLong(c.getColumnIndex(Events._ID))).toString();
				publishUpdate(new ExtensionData()
						.visible(true)
						.status(time)
						.expandedTitle(title)
						.expandedBody(expandedBody.toString())
						.icon(R.drawable.ic_dash_cal)
						.clickIntent(
								new Intent(Intent.ACTION_VIEW, Uri.parse(uri))));
				c.close();
			} else {
				publishUpdate(new ExtensionData().visible(false));
				c.close();
			}
		} else {
			publishUpdate(new ExtensionData().visible(false));
			c.close();
		}
	}
}
